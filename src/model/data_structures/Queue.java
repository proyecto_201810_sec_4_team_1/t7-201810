package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<T extends Comparable<T>> implements Iterable<T>{

	private NodeDL<T> foot, head; private int size;

	public Queue(){	foot = head = null; size = 0;}

	public boolean isEmpty() {return (size == 0);}

	public int size() {return size;}

	public Iterator<T> iterator()  {return new myIterator<T>(foot);}

	private class myIterator<T extends Comparable<T>> implements Iterator<T> {
		private NodeDL<T> current;
		public myIterator(NodeDL<T> first) {current = first;}
		public boolean hasNext()  { return current != null;}
		public T next() {	if (!hasNext()) throw new NoSuchElementException();
		T item = current.getItem();
		current = current.next(); 
		return item;
		}
	}

	public T dequeueLast() {
		T toret = null;
		if(foot != null) {size--;
		toret = foot.getItem();
		if(head == foot) foot = head = null;
		else {
			foot = foot.next();
			foot.setPrev(null);
		}
		}
		return toret;
	}

	public T dequeue() {
		T deq = null;
		if(head != null){	size--;
		deq = head.getItem();
		if(head == foot)
			foot = 	head = null;
		else{	head = head.previous();
		head.setNext(null);}
		}
		return deq;
	}

	public void queue(T item) {
		NodeDL<T> newNode = new NodeDL<T>(item);
		size++;
		if(foot== null)foot = head = newNode;
		else{	newNode.setNext(foot);
		foot.setPrev(newNode);
		foot = newNode;
		}
	}
}
