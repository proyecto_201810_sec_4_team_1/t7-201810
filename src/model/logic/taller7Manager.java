package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.value_objects.*;

public class taller7Manager {

	//__________ CONSTANTES PARA LA CARGA DE LOS ARCHIVOS_____________________
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "large"; //direccion estandar para acoplar a los viewers genéricos
	public static final String DIRECCION_LARGE= "./data/Large/taxi-trips-wrvz-psew-subset-0";
	public static final String COMPLEMENT_DIR = "-02-2017.json";
	public static final String DIRECCION_TEST_JSON = "./data/Test.json";
	public static final int CANTIDAD_ARCHIVOS = 7; //cantidad de archivos para la carga fragmentada del JSON large

	private RedBlackBST<Taxi> taxis = new RedBlackBST<>();

	// METODOS PARA LOS REQUERIMIENTOS

	//------------------INICIO DE LOS METODOS DE CARGA------------------------
	public void load(String ruta){	Gson g = new GsonBuilder().create();
	if(!ruta.equals(DIRECCION_LARGE_JSON)) cargaSML(g, ruta);
	else for(int i = 2; i<CANTIDAD_ARCHIVOS+2;i++) cargaSML(g, DIRECCION_LARGE + i + COMPLEMENT_DIR);}

	private void cargaSML(Gson g, String dirJson){
		try{FileInputStream stream = new FileInputStream(new File(dirJson));
		JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8")); reader.beginArray();

		while (reader.hasNext()) {
			Servicio srvc = g.fromJson(reader, Servicio.class);
			//carga un conjunto de servicios
			Taxi temp = new Taxi(srvc.getTaxiId(), srvc.getCompany());
			Taxi t = taxis.get(temp);
			if(t != null)t.addService(srvc);
			else {temp.addService(srvc);taxis.put(temp);}
		}
		reader.close(); System.out.println("hay en total: "+taxis.size()+" taxis");
		}catch(Exception e){System.out.println("Error en la carga del los archivos");e.printStackTrace();}
	}

	public RedBlackBST<Taxi> taxisReq1(){
		return  taxis;
	}
	public Taxi req2(String id) {
		return taxis.get(new Taxi(id, null));
	}
	public Queue<Taxi> req3(String Idmenor, String IdMayor){
		Taxi base = taxis.floor(  new Taxi(Idmenor, null));
		Taxi top  = taxis.ceiling(new Taxi(IdMayor, null));
		return (Queue<Taxi>) taxis.routeTo(base, top);
	}
}
