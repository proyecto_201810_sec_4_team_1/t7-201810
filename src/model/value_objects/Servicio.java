package model.value_objects;
/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>{	

	private String trip_id, taxi_id, company;
	//	______________________
	private int trip_seconds;
	private double trip_miles, trip_total;

	// point: dropoff_centroid_location, pickup_centroid_location
	//	 dropoff_census_tract
	//	extras, fare, payment_type, pickup_census_tract, dropoff_centroid_latitude
	//	dropoff_centroid_longitude, tips, tolls

	/**
	 * @return id - Trip_id
	 */
	public String getTripId(){return trip_id;}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {return taxi_id;}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {return trip_seconds;}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {return trip_miles;}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {return trip_total;}

	public boolean hasCompany() {return (this.company != null && this.company != "");}

	public boolean tieneDistancia() {return this.trip_miles > 0.0;}

	public String getCompany() {return this.company;}

	@Override
	public int compareTo(Servicio arg0) {
		return (this.trip_id.compareTo(arg0.getTripId())<0)?-1:
			(this.trip_id.compareTo(arg0.getTripId())>0)?1:0;}
}
