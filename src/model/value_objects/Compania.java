package model.value_objects;

public class Compania implements Comparable<Compania> {

	private String nombre;

	public Compania(String nombre){this.nombre = nombre;}

	public String getNombre() {return nombre;}

	public void setNombre(String nombre) {this.nombre = nombre;}

	@Override
	public int compareTo(Compania o) {
		return (this.nombre != null)?this.nombre.compareTo(o.getNombre()):0;
	}
}
