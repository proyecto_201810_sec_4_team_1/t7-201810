package model.value_objects;

import model.data_structures.RedBlackBST;

public class Taxi implements Comparable<Taxi>{

	private String taxiId;
	private String compania;
	private RedBlackBST<Servicio> servicios;
	private double totalCash, totalMiles, tripsTime;

	public Taxi(String taxiId, String compania){
		this.servicios = new RedBlackBST<>();
		this.taxiId = taxiId;
		this.compania = (compania!= null)?compania:"Independent Owner";	
		totalCash = totalMiles = tripsTime = 0;
	}

	public void addService(Servicio s) {if(s!= null) {totalCash += s.getTripTotal(); totalMiles += s.getTripMiles();  tripsTime += s.getTripSeconds(); servicios.put(s);}}
	public int cantServicios() {return servicios.size();}
	public String getCompany() {return this.compania;   }
	public String id() {return taxiId;}
	public double getCash()    {return totalCash;       }
	public double getMiles()   {return totalMiles;      }
	public double getTimes()   {return tripsTime;       }

	@Override public int compareTo(Taxi arg0) {return this.taxiId.compareTo(arg0.taxiId);}
	@Override public String toString() {return "Empresa: "+ getCompany() + "\n" +
			"Cantidad de servicios: " + servicios.size() + "\n"+
			"total de dinero: " + getCash() + " dolares \n"+
			"tiempo invertido: "+ Math.round(getTimes()/3600) + " horas \n"+
			"distancia total: " + getMiles() + " millas";}
}
