package viewer;

import java.util.Scanner;

import model.data_structures.Queue;
import model.logic.taller7Manager;
import model.value_objects.Taxi;

public class Viewer {
	private static final Timer t = new Timer();
	private static final taller7Manager manager = new taller7Manager();

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		while(!fin) {	printMenu();			
		int opcion = sc.nextInt();
		switch (opcion) {
		case 1:	
			printMenuLoad();
			int optionCargar = sc.nextInt();
			//
			String linkJson = "";
			switch (optionCargar){
			case 1:	linkJson = manager.DIRECCION_SMALL_JSON  ;	break; //Direcci�n del archivo de datos peque�o
			case 2:	linkJson = manager.DIRECCION_MEDIUM_JSON ;	break; //Direcci�n del archivo de datos mediano
			case 3:	linkJson = manager.DIRECCION_LARGE_JSON  ;	break; //Direcci�n del archivo de datos grande
			case 4:	linkJson = manager.DIRECCION_TEST_JSON   ;	break;} // Test con pocos taxis pero varios casos
			System.out.println("Datos cargados: " + linkJson);
			t.start();
			manager.load(linkJson);
			t.end();	break;
		case 2:	//requerimiento taller

			Queue<Taxi> listaTaxis = (Queue<Taxi>) manager.taxisReq1().values();
			System.out.println("a) Hay " + listaTaxis.size() +" taxis cargados con al menos un servicio");
			System.out.println("b) la altura del �rbol creado es de: " + manager.taxisReq1().height() + " niveles");
			System.out.println("c) la altura promedio para la busqueda de un taxi es: " + (manager.taxisReq1().height() + manager.taxisReq1().minheight())/2);
			System.out.println("d) el �rbol m�s alto que se podr�a crear con " + listaTaxis.size()+" taxis tendr�a una altura de: " + 2*(Math.ceil(Math.log10(listaTaxis.size() +1)/Math.log10(2))));
			System.out.println("e) el �rbol (2-3) m�s alto que se podr�a crear con " + listaTaxis.size()+" taxis tendr�a una altura de: " + 2*(Math.ceil(Math.log10(listaTaxis.size() +1)/Math.log10(3))));
			System.out.println("f) el �rbol (2-3) m�s bajo que se podr�a crear con " + listaTaxis.size()+" taxis tendr�a una altura de: " + (Math.ceil(Math.log10(listaTaxis.size() +1)/Math.log10(3))));
			System.out.println("g) comentario: viendo que en general la altura del arbol es menor en la creacion de los arboles, puedo de ello asumir que ocurrir� en mas de una ocasi�n \n"
					+ "adem�s se ve que pese a que se cargan miles de taxis y por ende decenas de miles de servicios, la busqueda es bastante corta. \n"
					+ "y compar�ndolo con respecto a los arboles (2-3) se ve  que el 2-3 puede tener ventajas con respecto a la busqueda que en s� radican en unas , o extremando, docenas de saltos, pero dado que "
					+ "no s�lo \n depende de la funci�n sino tambien de la implementaci�n y dado que la implementacion del 2-3 no es tan intuitiva, siento que es mejor usar los arboles rojo-negro \n "
					+ "que aunque sacrifica unos saltos, mejora al ser comprensible en codigo");

			break;
		case 3:	//requerimiento taller

			System.out.println("Ingrese el id del taxi buscado   ejemplo: a0aeba38e8e4402fe3e71d4ba092946618b9513e4ba7b13f8ff3ca8ed27a152d64b7189773d634ad77e545871e7ce6a5d0857376544e8c55dbb73e4d988fd375");
			String idReq2 = sc.next();
			Taxi req2 = manager.req2(idReq2);
			if(req2 != null)System.out.println(req2.toString());
			else System.out.println("No encontr� el taxi especificado :(");

			break;

		case 4:	//�ltimo requerimiento taller

			System.out.println("Ingrese el id del taxi base");
			String idBaseReq3 = sc.next();
			System.out.println("Ingrese el id del taxi mayor");
			String idCotaReq3 = sc.next();
			Queue<Taxi> req3 = manager.req3(idBaseReq3, idCotaReq3);
			if(req3 != null)for (Taxi taxi : req3) if(taxi.id() != "NaN" && taxi.id() != null)System.out.println(taxi.id() + "\n");

			break;
		case 5:	 fin = true; sc.close(); break;
		}}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 2----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Cargar toda la informacion del sistema de una fuente de datos (small, medium o large).");

		System.out.println("\n Requerimientos:\n");
		System.out.println("2. Mostrar la siguiente informaci�n a partir del �rbol Red-Black construido:\r\n" + 
				"a.\r Total de taxis en el �rbol\r\n" + 
				"b.\r Altura (real) del �rbol\r\n" + 
				"c.\r Altura promedio para buscar un taxi existente en el �rbol (Ayuda: Calcular el promedio de \r las alturas de los taxis en el �rbol)\r\n" + 
				"d.\r Altura te�rica del  �rbol Red-Black m�s alto que se  podr�a  construir  con el  mismo  n�mero de taxis\r\n" + 
				"e.\r Altura te�rica del �rbol 2-3 m�s alto que se podr�a construir si se agrega el mismo n�mero de taxis\r\n" + 
				"f.\r Altura te�rica del �rbol 2-3 m�s bajo que se podr�a construir si se agrega el mismo n�mero de taxis\r\n" + 
				"g.\r Comentario de an�lisis de la altura de su �rbol Red-Black (2.b) con respecto a las alturas de los �rboles 2.d, 2.e y 2.f. \r Comentar ventaja(s) y desventaja(s)"
				+ " de su �rbol con respecto a los otros posibles �rboles construidos con la misma informaci�n");
		System.out.println("3. Consultar la informaci�n asociada a un taxi dado su Id");
		System.out.println("4. los Id's de los taxis con un Id entre un rango de Id's");
	}
	private static void printMenuLoad() {
		System.out.println("-- �Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- 4. Test");
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}
}
